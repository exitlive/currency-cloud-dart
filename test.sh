#!/bin/bash

dart format --set-exit-if-changed -o none .
dart analyze --fatal-infos --fatal-warnings
dart test