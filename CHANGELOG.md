# Changelog

## 2.0.0

- Null safety

## 1.1.0

- Upgrade dependencies
- Use pedantic for linter

## 1.0.2

- Add missing underscore for certain Beneficiary keys

## 1.0.1

- Fix wrong cast in the references data API call
- Switch to `json_serialize` instead of `codable` for JSON (de)serialization

## 1.0.0

- Use classes instead of maps for all API messages
- Remove money dependency
- Revert to use String of currency codes again

## 0.3.0

- Fix all implicit type casts and cleanup for dart 2.0

## 0.2.0

- Add missing pub dependency
- `PaymentsApi.create()` now uses `PaymentType` instead of a String
- API calls with currencies as arguments use [`Currency` from money](https://pub.dartlang.org/packages/money)
  now instead of String

## 0.1.0

- Add grinder for formatting and testing
- Add ci for gitlab
- Remove defunct authentication test
- Remove integration tests
- Remove debug output
- Update gitignore

## 0.0.8

- switch to new api url

## 0.0.7

- add balance api (retrieve)

## 0.0.1

- Initial version, created by Stagehand
