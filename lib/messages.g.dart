// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'messages.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Balance _$BalanceFromJson(Map<String, dynamic> json) => Balance(
      id: json['id'] as String,
      accountId: json['account_id'] as String,
      currency: json['currency'] as String,
      amount: decimalNonNullFromJson(json['amount'] as String),
      createdAt: DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
    );

Map<String, dynamic> _$BalanceToJson(Balance instance) {
  final val = <String, dynamic>{
    'id': instance.id,
    'account_id': instance.accountId,
    'currency': instance.currency,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('amount', decimalNonNullToJson(instance.amount));
  val['created_at'] = instance.createdAt.toIso8601String();
  writeNotNull('updated_at', instance.updatedAt?.toIso8601String());
  return val;
}

CreateBeneficiary _$CreateBeneficiaryFromJson(Map<String, dynamic> json) =>
    CreateBeneficiary(
      name: json['name'] as String,
      bankAccountHolderName: json['bank_account_holder_name'] as String,
      bankCountry: json['bank_country'] as String,
      currency: json['currency'] as String,
      email: json['email'] as String?,
      beneficiaryAddress: json['beneficiary_address'] as String?,
      beneficiaryCountry: json['beneficiary_country'] as String?,
      accountNumber: json['account_number'] as String?,
      routingCodeType1: _$enumDecodeNullable(
          _$RoutingCodeTypeEnumMap, json['routing_code_type_1']),
      routingCodeValue1: json['routing_code_value_1'] as String?,
      routingCodeType2: _$enumDecodeNullable(
          _$RoutingCodeTypeEnumMap, json['routing_code_type_2']),
      routingCodeValue2: json['routing_code_value_2'] as String?,
      bicSwift: json['bic_swift'] as String?,
      iban: json['iban'] as String?,
      defaultBeneficiary: boolFromJson(json['default_beneficiary']),
      bankAddress: json['bank_address'] as String?,
      bankName: json['bank_name'] as String?,
      bankAccountType: _$enumDecodeNullable(
          _$BankAccountTypeEnumMap, json['bank_account_type']),
      beneficiaryEntityType: _$enumDecodeNullable(
          _$EntityTypeEnumMap, json['beneficiary_entity_type']),
      beneficiaryCompanyName: json['beneficiary_company_name'] as String?,
      beneficiaryFirstName: json['beneficiary_first_name'] as String?,
      beneficiaryLastName: json['beneficiary_last_name'] as String?,
      beneficiaryCity: json['beneficiary_city'] as String?,
      beneficiaryPostcode: json['beneficiary_postcode'] as String?,
      beneficiaryStateOrProvince:
          json['beneficiary_state_or_province'] as String?,
      beneficiaryDateOfBirth: json['beneficiary_date_of_birth'] as String?,
      beneficiaryIdentificationType: _$enumDecodeNullable(
          _$EntityTypeEnumMap, json['beneficiary_identification_type']),
      beneficiaryIdentificationValue:
          json['beneficiary_identification_value'] as String?,
      paymentTypes: (json['payment_types'] as List<dynamic>?)
          ?.map((e) => _$enumDecodeNullable(_$PaymentTypeEnumMap, e))
          .toList(),
      onBehalfOf: json['on_behalf_of'] as String?,
    );

Map<String, dynamic> _$CreateBeneficiaryToJson(CreateBeneficiary instance) {
  final val = <String, dynamic>{
    'name': instance.name,
    'bank_account_holder_name': instance.bankAccountHolderName,
    'bank_country': instance.bankCountry,
    'currency': instance.currency,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('email', instance.email);
  writeNotNull('beneficiary_address', instance.beneficiaryAddress);
  writeNotNull('beneficiary_country', instance.beneficiaryCountry);
  writeNotNull('account_number', instance.accountNumber);
  writeNotNull('routing_code_type_1',
      _$RoutingCodeTypeEnumMap[instance.routingCodeType1]);
  writeNotNull('routing_code_value_1', instance.routingCodeValue1);
  writeNotNull('routing_code_type_2',
      _$RoutingCodeTypeEnumMap[instance.routingCodeType2]);
  writeNotNull('routing_code_value_2', instance.routingCodeValue2);
  writeNotNull('bic_swift', instance.bicSwift);
  writeNotNull('iban', instance.iban);
  writeNotNull('default_beneficiary', boolToJson(instance.defaultBeneficiary));
  writeNotNull('bank_address', instance.bankAddress);
  writeNotNull('bank_name', instance.bankName);
  writeNotNull(
      'bank_account_type', _$BankAccountTypeEnumMap[instance.bankAccountType]);
  writeNotNull('beneficiary_entity_type',
      _$EntityTypeEnumMap[instance.beneficiaryEntityType]);
  writeNotNull('beneficiary_company_name', instance.beneficiaryCompanyName);
  writeNotNull('beneficiary_first_name', instance.beneficiaryFirstName);
  writeNotNull('beneficiary_last_name', instance.beneficiaryLastName);
  writeNotNull('beneficiary_city', instance.beneficiaryCity);
  writeNotNull('beneficiary_postcode', instance.beneficiaryPostcode);
  writeNotNull(
      'beneficiary_state_or_province', instance.beneficiaryStateOrProvince);
  writeNotNull('beneficiary_date_of_birth', instance.beneficiaryDateOfBirth);
  writeNotNull('beneficiary_identification_type',
      _$EntityTypeEnumMap[instance.beneficiaryIdentificationType]);
  writeNotNull('beneficiary_identification_value',
      instance.beneficiaryIdentificationValue);
  writeNotNull('payment_types',
      instance.paymentTypes?.map((e) => _$PaymentTypeEnumMap[e]).toList());
  writeNotNull('on_behalf_of', instance.onBehalfOf);
  return val;
}

K _$enumDecode<K, V>(
  Map<K, V> enumValues,
  Object? source, {
  K? unknownValue,
}) {
  if (source == null) {
    throw ArgumentError(
      'A value must be provided. Supported values: '
      '${enumValues.values.join(', ')}',
    );
  }

  return enumValues.entries.singleWhere(
    (e) => e.value == source,
    orElse: () {
      if (unknownValue == null) {
        throw ArgumentError(
          '`$source` is not one of the supported values: '
          '${enumValues.values.join(', ')}',
        );
      }
      return MapEntry(unknownValue, enumValues.values.first);
    },
  ).key;
}

K? _$enumDecodeNullable<K, V>(
  Map<K, V> enumValues,
  dynamic source, {
  K? unknownValue,
}) {
  if (source == null) {
    return null;
  }
  return _$enumDecode<K, V>(enumValues, source, unknownValue: unknownValue);
}

const _$RoutingCodeTypeEnumMap = {
  RoutingCodeType.sortCode: 'sort_code',
  RoutingCodeType.aba: 'aba',
  RoutingCodeType.bsbCode: 'bsb_code',
  RoutingCodeType.institutionNo: 'institution_no',
  RoutingCodeType.bankCode: 'bank_code',
  RoutingCodeType.branchCode: 'branch_code',
  RoutingCodeType.clabe: 'clabe',
  RoutingCodeType.cnaps: 'cnaps',
  RoutingCodeType.ifsc: 'ifsc',
};

const _$BankAccountTypeEnumMap = {
  BankAccountType.checkings: 'checkings',
  BankAccountType.savings: 'savings',
};

const _$EntityTypeEnumMap = {
  EntityType.individual: 'individual',
  EntityType.company: 'company',
};

const _$PaymentTypeEnumMap = {
  PaymentType.regular: 'regular',
  PaymentType.priority: 'priority',
};

Beneficiary _$BeneficiaryFromJson(Map<String, dynamic> json) => Beneficiary(
      id: json['id'] as String,
      bankAccountHolderName: json['bank_account_holder_name'] as String,
      name: json['name'] as String,
      email: json['email'] as String?,
      paymentTypes: (json['payment_types'] as List<dynamic>?)
          ?.map((e) => _$enumDecodeNullable(_$PaymentTypeEnumMap, e))
          .toList(),
      beneficiaryAddress: (json['beneficiary_address'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      beneficiaryCountry: json['beneficiary_country'] as String?,
      beneficiaryEntityType: _$enumDecodeNullable(
          _$EntityTypeEnumMap, json['beneficiary_entity_type']),
      beneficiaryCompanyName: json['beneficiary_company_name'] as String?,
      beneficiaryFirstName: json['beneficiary_first_name'] as String?,
      beneficiaryLastName: json['beneficiary_last_name'] as String?,
      beneficiaryCity: json['beneficiary_city'] as String?,
      beneficiaryPostcode: json['beneficiary_postcode'] as String?,
      beneficiaryStateOrProvince:
          json['beneficiary_state_or_province'] as String?,
      beneficiaryDateOfBirth: json['beneficiary_date_of_birth'] as String?,
      beneficiaryIdentificationType: _$enumDecodeNullable(
          _$EntityTypeEnumMap, json['beneficiary_identification_type']),
      beneficiaryIdentificationValue:
          json['beneficiary_identification_value'] as String?,
      bankCountry: json['bank_country'] as String?,
      bankName: json['bank_name'] as String?,
      bankAddress: (json['bank_address'] as List<dynamic>?)
          ?.map((e) => e as String)
          .toList(),
      bankAccountType: _$enumDecodeNullable(
          _$BankAccountTypeEnumMap, json['bank_account_type']),
      onBehalfOf: json['on_behalf_of'] as String?,
      currency: json['currency'] as String?,
      accountNumber: json['account_number'] as String?,
      routingCodeType1: _$enumDecodeNullable(
          _$RoutingCodeTypeEnumMap, json['routing_code_type_1']),
      routingCodeValue1: json['routing_code_value_1'] as String?,
      routingCodeType2: _$enumDecodeNullable(
          _$RoutingCodeTypeEnumMap, json['routing_code_type_2']),
      routingCodeValue2: json['routing_code_value_2'] as String?,
      bicSwift: json['bic_swift'] as String?,
      iban: json['iban'] as String?,
      defaultBeneficiary: boolFromJson(json['default_beneficiary']),
      creatorContactId: json['creator_contact_id'] as String?,
      beneficiaryExternalReference:
          json['beneficiary_external_reference'] as String?,
      createdAt: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
    );

Map<String, dynamic> _$BeneficiaryToJson(Beneficiary instance) {
  final val = <String, dynamic>{
    'id': instance.id,
    'bank_account_holder_name': instance.bankAccountHolderName,
    'name': instance.name,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('email', instance.email);
  writeNotNull('payment_types',
      instance.paymentTypes?.map((e) => _$PaymentTypeEnumMap[e]).toList());
  writeNotNull('beneficiary_address', instance.beneficiaryAddress);
  writeNotNull('beneficiary_country', instance.beneficiaryCountry);
  writeNotNull('beneficiary_entity_type',
      _$EntityTypeEnumMap[instance.beneficiaryEntityType]);
  writeNotNull('beneficiary_company_name', instance.beneficiaryCompanyName);
  writeNotNull('beneficiary_first_name', instance.beneficiaryFirstName);
  writeNotNull('beneficiary_last_name', instance.beneficiaryLastName);
  writeNotNull('beneficiary_city', instance.beneficiaryCity);
  writeNotNull('beneficiary_postcode', instance.beneficiaryPostcode);
  writeNotNull(
      'beneficiary_state_or_province', instance.beneficiaryStateOrProvince);
  writeNotNull('beneficiary_date_of_birth', instance.beneficiaryDateOfBirth);
  writeNotNull('beneficiary_identification_type',
      _$EntityTypeEnumMap[instance.beneficiaryIdentificationType]);
  writeNotNull('beneficiary_identification_value',
      instance.beneficiaryIdentificationValue);
  writeNotNull('bank_country', instance.bankCountry);
  writeNotNull('bank_name', instance.bankName);
  writeNotNull('bank_address', instance.bankAddress);
  writeNotNull(
      'bank_account_type', _$BankAccountTypeEnumMap[instance.bankAccountType]);
  writeNotNull('on_behalf_of', instance.onBehalfOf);
  writeNotNull('currency', instance.currency);
  writeNotNull('account_number', instance.accountNumber);
  writeNotNull('routing_code_type_1',
      _$RoutingCodeTypeEnumMap[instance.routingCodeType1]);
  writeNotNull('routing_code_value_1', instance.routingCodeValue1);
  writeNotNull('routing_code_type_2',
      _$RoutingCodeTypeEnumMap[instance.routingCodeType2]);
  writeNotNull('routing_code_value_2', instance.routingCodeValue2);
  writeNotNull('bic_swift', instance.bicSwift);
  writeNotNull('iban', instance.iban);
  writeNotNull('default_beneficiary', boolToJson(instance.defaultBeneficiary));
  writeNotNull('creator_contact_id', instance.creatorContactId);
  writeNotNull(
      'beneficiary_external_reference', instance.beneficiaryExternalReference);
  writeNotNull('created_at', instance.createdAt?.toIso8601String());
  writeNotNull('updated_at', instance.updatedAt?.toIso8601String());
  return val;
}

CreateConversion _$CreateConversionFromJson(Map<String, dynamic> json) =>
    CreateConversion(
      buyCurrency: json['buy_currency'] as String,
      sellCurrency: json['sell_currency'] as String,
      fixedSide: _$enumDecode(_$FixedSideEnumMap, json['fixed_side']),
      amount: decimalNonNullFromJson(json['amount'] as String),
      termAgreement: boolNonNullFromJson(json['term_agreement']),
      reason: json['reason'] as String?,
    );

Map<String, dynamic> _$CreateConversionToJson(CreateConversion instance) {
  final val = <String, dynamic>{
    'buy_currency': instance.buyCurrency,
    'sell_currency': instance.sellCurrency,
    'fixed_side': _$FixedSideEnumMap[instance.fixedSide],
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('amount', decimalNonNullToJson(instance.amount));
  writeNotNull('term_agreement', boolNonNullToJson(instance.termAgreement));
  writeNotNull('reason', instance.reason);
  return val;
}

const _$FixedSideEnumMap = {
  FixedSide.buy: 'buy',
  FixedSide.sell: 'sell',
};

Conversion _$ConversionFromJson(Map<String, dynamic> json) => Conversion(
      id: json['id'] as String,
      settlementDate: json['settlement_date'] == null
          ? null
          : DateTime.parse(json['settlement_date'] as String),
      conversionDate: json['conversion_date'] == null
          ? null
          : DateTime.parse(json['conversion_date'] as String),
      createdAt: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
      status: json['status'] as String?,
      buyCurrency: json['buy_currency'] as String?,
      sellCurrency: json['sell_currency'] as String?,
      clientBuyAmount: decimalFromJson(json['client_buy_amount'] as String?),
      clientSellAmount: decimalFromJson(json['client_sell_amount'] as String?),
      fixedSide: _$enumDecodeNullable(_$FixedSideEnumMap, json['fixed_side']),
      coreRate: decimalFromJson(json['core_rate'] as String?),
    );

Map<String, dynamic> _$ConversionToJson(Conversion instance) {
  final val = <String, dynamic>{
    'id': instance.id,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('settlement_date', instance.settlementDate?.toIso8601String());
  writeNotNull('conversion_date', instance.conversionDate?.toIso8601String());
  writeNotNull('created_at', instance.createdAt?.toIso8601String());
  writeNotNull('updated_at', instance.updatedAt?.toIso8601String());
  writeNotNull('status', instance.status);
  writeNotNull('buy_currency', instance.buyCurrency);
  writeNotNull('sell_currency', instance.sellCurrency);
  writeNotNull('client_buy_amount', decimalToJson(instance.clientBuyAmount));
  writeNotNull('client_sell_amount', decimalToJson(instance.clientSellAmount));
  writeNotNull('fixed_side', _$FixedSideEnumMap[instance.fixedSide]);
  writeNotNull('core_rate', decimalToJson(instance.coreRate));
  return val;
}

CreatePayment _$CreatePaymentFromJson(Map<String, dynamic> json) =>
    CreatePayment(
      currency: json['currency'] as String,
      beneficiaryId: json['beneficiary_id'] as String,
      amount: decimalNonNullFromJson(json['amount'] as String),
      reason: json['reason'] as String,
      reference: json['reference'] as String,
      paymentDate: json['payment_date'] as String?,
      paymentType:
          _$enumDecodeNullable(_$PaymentTypeEnumMap, json['payment_type']) ??
              PaymentType.regular,
      conversionId: json['conversion_id'] as String?,
      payerEntityType:
          _$enumDecodeNullable(_$EntityTypeEnumMap, json['payer_entity_type']),
      payerCompanyName: json['payer_company_name'] as String?,
      payerFirstName: json['payer_first_name'] as String?,
      payerLastName: json['payer_last_name'] as String?,
      payerCity: json['payer_city'] as String?,
      payerAddress: json['payer_address'] as String?,
      payerPostcode: json['payer_postcode'] as String?,
      payerStateOrProvince: json['payer_state_or_province'] as String?,
      payerCountry: json['payer_country'] as String?,
      payerDateOfBirth: json['payer_date_of_birth'] as String?,
      payerIdentificationType: _$enumDecodeNullable(
          _$IdentificationTypeEnumMap, json['payer_identification_type']),
      payerIdentificationValue: json['payer_identification_value'] as String?,
      uniqueRequestId: json['unique_request_id'] as String?,
      ultimateBeneficiaryName: json['ultimate_beneficiary_name'] as String?,
      onBehalfOf: json['on_behalf_of'] as String?,
    );

Map<String, dynamic> _$CreatePaymentToJson(CreatePayment instance) {
  final val = <String, dynamic>{
    'currency': instance.currency,
    'beneficiary_id': instance.beneficiaryId,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('amount', decimalNonNullToJson(instance.amount));
  val['reason'] = instance.reason;
  val['reference'] = instance.reference;
  writeNotNull('payment_date', instance.paymentDate);
  writeNotNull('payment_type', _$PaymentTypeEnumMap[instance.paymentType]);
  writeNotNull('conversion_id', instance.conversionId);
  writeNotNull(
      'payer_entity_type', _$EntityTypeEnumMap[instance.payerEntityType]);
  writeNotNull('payer_company_name', instance.payerCompanyName);
  writeNotNull('payer_first_name', instance.payerFirstName);
  writeNotNull('payer_last_name', instance.payerLastName);
  writeNotNull('payer_city', instance.payerCity);
  writeNotNull('payer_address', instance.payerAddress);
  writeNotNull('payer_postcode', instance.payerPostcode);
  writeNotNull('payer_state_or_province', instance.payerStateOrProvince);
  writeNotNull('payer_country', instance.payerCountry);
  writeNotNull('payer_date_of_birth', instance.payerDateOfBirth);
  writeNotNull('payer_identification_type',
      _$IdentificationTypeEnumMap[instance.payerIdentificationType]);
  writeNotNull('payer_identification_value', instance.payerIdentificationValue);
  writeNotNull('unique_request_id', instance.uniqueRequestId);
  writeNotNull('ultimate_beneficiary_name', instance.ultimateBeneficiaryName);
  writeNotNull('on_behalf_of', instance.onBehalfOf);
  return val;
}

const _$IdentificationTypeEnumMap = {
  IdentificationType.none: 'none',
  IdentificationType.driversLicense: 'drivers_license',
  IdentificationType.socialSecurityNumber: 'social_security_number',
  IdentificationType.greenCard: 'green_card',
  IdentificationType.passport: 'passport',
  IdentificationType.visa: 'visa',
  IdentificationType.matriculaConsular: 'matricula_consular',
  IdentificationType.registroFederalDeContribuyentes:
      'registro_federal_de_contribuyentes',
  IdentificationType.claveUnicaDeRegistroDePoblacion:
      'clave_unica_de_registro_de_poblacion',
  IdentificationType.credentialDeElector: 'credential_de_elector',
  IdentificationType.socialInsuranceNumber: 'social_insurance_number',
  IdentificationType.citizenshipPapers: 'citizenship_papers',
  IdentificationType.driversLicenseCanadian: 'drivers_license_canadian',
  IdentificationType.existingCreditCardDetails: 'existing_credit_card_details',
  IdentificationType.employerIdentificationNumber:
      'employer_identification_number',
  IdentificationType.nationalId: 'national_id',
  IdentificationType.incorporationNumber: 'incorporation_number',
  IdentificationType.others: 'others',
};

Payment _$PaymentFromJson(Map<String, dynamic> json) => Payment(
      id: json['id'] as String,
      amount: decimalFromJson(json['amount'] as String?),
      beneficiaryId: json['beneficiary_id'] as String?,
      currency: json['currency'] as String?,
      reference: json['reference'] as String?,
      reason: json['reason'] as String?,
      status: json['status'] as String?,
      creatorContactId: json['creator_contact_id'] as String?,
      paymentType:
          _$enumDecodeNullable(_$PaymentTypeEnumMap, json['payment_type']),
      paymentDate: json['payment_date'] as String?,
      transferredAt: json['transferred_at'] as String?,
      authorisationStepsRequired:
          json['authorisation_steps_required'] as String?,
      lastUpdaterContactId: json['last_updater_contact_id'] as String?,
      shortReference: json['short_reference'] as String?,
      conversionId: json['conversion_id'] as String?,
      failureReason: json['failure_reason'] as String?,
      payerId: json['payer_id'] as String?,
      payerDetailsSource: json['payer_details_source'] as String?,
      createdAt: json['created_at'] == null
          ? null
          : DateTime.parse(json['created_at'] as String),
      updatedAt: json['updated_at'] == null
          ? null
          : DateTime.parse(json['updated_at'] as String),
      paymentGroupId: json['payment_group_id'] as String?,
      uniqueRequestId: json['unique_request_id'] as String?,
      failureReturnedAmount:
          decimalFromJson(json['failure_returned_amount'] as String?),
      ultimateBeneficiaryName: json['ultimate_beneficiary_name'] as String?,
    );

Map<String, dynamic> _$PaymentToJson(Payment instance) {
  final val = <String, dynamic>{
    'id': instance.id,
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('amount', decimalToJson(instance.amount));
  writeNotNull('beneficiary_id', instance.beneficiaryId);
  writeNotNull('currency', instance.currency);
  writeNotNull('reference', instance.reference);
  writeNotNull('reason', instance.reason);
  writeNotNull('status', instance.status);
  writeNotNull('creator_contact_id', instance.creatorContactId);
  writeNotNull('payment_type', _$PaymentTypeEnumMap[instance.paymentType]);
  writeNotNull('payment_date', instance.paymentDate);
  writeNotNull('transferred_at', instance.transferredAt);
  writeNotNull(
      'authorisation_steps_required', instance.authorisationStepsRequired);
  writeNotNull('last_updater_contact_id', instance.lastUpdaterContactId);
  writeNotNull('short_reference', instance.shortReference);
  writeNotNull('conversion_id', instance.conversionId);
  writeNotNull('failure_reason', instance.failureReason);
  writeNotNull('payer_id', instance.payerId);
  writeNotNull('payer_details_source', instance.payerDetailsSource);
  writeNotNull('created_at', instance.createdAt?.toIso8601String());
  writeNotNull('updated_at', instance.updatedAt?.toIso8601String());
  writeNotNull('payment_group_id', instance.paymentGroupId);
  writeNotNull('unique_request_id', instance.uniqueRequestId);
  writeNotNull(
      'failure_returned_amount', decimalToJson(instance.failureReturnedAmount));
  writeNotNull('ultimate_beneficiary_name', instance.ultimateBeneficiaryName);
  return val;
}

DetailedRateRequest _$DetailedRateRequestFromJson(Map<String, dynamic> json) =>
    DetailedRateRequest(
      buyCurrency: json['buy_currency'] as String,
      sellCurrency: json['sell_currency'] as String,
      fixedSide: _$enumDecode(_$FixedSideEnumMap, json['fixed_side']),
      amount: decimalNonNullFromJson(json['amount'] as String),
      onBehalfOf: json['on_behalf_of'] as String?,
      conversionDate: json['conversion_date'] == null
          ? null
          : DateTime.parse(json['conversion_date'] as String),
    );

Map<String, dynamic> _$DetailedRateRequestToJson(DetailedRateRequest instance) {
  final val = <String, dynamic>{
    'buy_currency': instance.buyCurrency,
    'sell_currency': instance.sellCurrency,
    'fixed_side': _$FixedSideEnumMap[instance.fixedSide],
  };

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('amount', decimalNonNullToJson(instance.amount));
  writeNotNull('on_behalf_of', instance.onBehalfOf);
  writeNotNull('conversion_date', instance.conversionDate?.toIso8601String());
  return val;
}

Rate _$RateFromJson(Map<String, dynamic> json) => Rate()
  ..settlementCutOffTime = json['settlement_cut_off_time'] == null
      ? null
      : DateTime.parse(json['settlement_cut_off_time'] as String)
  ..currencyPair = json['currency_pair'] as String?
  ..clientBuyCurrency = json['client_buy_currency'] as String?
  ..clientSellCurrency = json['client_sell_currency'] as String?
  ..clientBuyAmount = decimalFromJson(json['client_buy_amount'] as String?)
  ..clientSellAmount = decimalFromJson(json['client_sell_amount'] as String?)
  ..fixedSide = _$enumDecodeNullable(_$FixedSideEnumMap, json['fixed_side'])
  ..clientRate = decimalFromJson(json['client_rate'] as String?)
  ..partnerRate = decimalFromJson(json['partner_rate'] as String?)
  ..coreRate = decimalFromJson(json['core_rate'] as String?)
  ..depositRequired = boolFromJson(json['deposit_required'])
  ..depositAmount = decimalFromJson(json['deposit_amount'] as String?)
  ..depositCurrency = json['deposit_currency'] as String?
  ..midMarketRate = decimalFromJson(json['mid_market_rate'] as String?);

Map<String, dynamic> _$RateToJson(Rate instance) {
  final val = <String, dynamic>{};

  void writeNotNull(String key, dynamic value) {
    if (value != null) {
      val[key] = value;
    }
  }

  writeNotNull('settlement_cut_off_time',
      instance.settlementCutOffTime?.toIso8601String());
  writeNotNull('currency_pair', instance.currencyPair);
  writeNotNull('client_buy_currency', instance.clientBuyCurrency);
  writeNotNull('client_sell_currency', instance.clientSellCurrency);
  writeNotNull('client_buy_amount', decimalToJson(instance.clientBuyAmount));
  writeNotNull('client_sell_amount', decimalToJson(instance.clientSellAmount));
  writeNotNull('fixed_side', _$FixedSideEnumMap[instance.fixedSide]);
  writeNotNull('client_rate', decimalToJson(instance.clientRate));
  writeNotNull('partner_rate', decimalToJson(instance.partnerRate));
  writeNotNull('core_rate', decimalToJson(instance.coreRate));
  writeNotNull('deposit_required', boolToJson(instance.depositRequired));
  writeNotNull('deposit_amount', decimalToJson(instance.depositAmount));
  writeNotNull('deposit_currency', instance.depositCurrency);
  writeNotNull('mid_market_rate', decimalToJson(instance.midMarketRate));
  return val;
}
