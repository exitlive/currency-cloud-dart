import 'package:json_annotation/json_annotation.dart';

enum BankAccountType { checkings, savings }

enum EntityType { individual, company }

enum IdentificationType {
  none,
  @JsonValue('drivers_license')
  driversLicense,
  @JsonValue('social_security_number')
  socialSecurityNumber,
  @JsonValue('green_card')
  greenCard,
  passport,
  visa,
  @JsonValue('matricula_consular')
  matriculaConsular,
  @JsonValue('registro_federal_de_contribuyentes')
  registroFederalDeContribuyentes,
  @JsonValue('clave_unica_de_registro_de_poblacion')
  claveUnicaDeRegistroDePoblacion,
  @JsonValue('credential_de_elector')
  credentialDeElector,
  @JsonValue('social_insurance_number')
  socialInsuranceNumber,
  @JsonValue('citizenship_papers')
  citizenshipPapers,
  @JsonValue('drivers_license_canadian')
  driversLicenseCanadian,
  @JsonValue('existing_credit_card_details')
  existingCreditCardDetails,
  @JsonValue('employer_identification_number')
  employerIdentificationNumber,
  @JsonValue('national_id')
  nationalId,
  @JsonValue('incorporation_number')
  incorporationNumber,
  others,
}

enum FixedSide { buy, sell }

enum PaymentType { regular, priority }

enum RoutingCodeType {
  @JsonValue('sort_code')
  sortCode,
  aba,
  @JsonValue('bsb_code')
  bsbCode,
  @JsonValue('institution_no')
  institutionNo,
  @JsonValue('bank_code')
  bankCode,
  @JsonValue('branch_code')
  branchCode,
  clabe,
  cnaps,
  ifsc
}
