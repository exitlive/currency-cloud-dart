import 'package:decimal/decimal.dart';
import 'package:json_annotation/json_annotation.dart';

import 'types.dart';

export 'types.dart';

part 'messages.g.dart';
part 'src/api/messages/balance.dart';
part 'src/api/messages/beneficiary.dart';
part 'src/api/messages/conversion.dart';
part 'src/api/messages/json_converters.dart';
part 'src/api/messages/payment.dart';
part 'src/api/messages/rate.dart';
