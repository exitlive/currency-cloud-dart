part of currency_cloud;

/// Base Class for all Currency Cloud APIs (according to Currency Cloud documented APIs)
/// Every API has to get the same instance of AuthToken, which is the only shared
/// information between all instances of [CurrencyCloudApi].
abstract class CurrencyCloudApi {
  CurrencyCloudClient client;

  CurrencyCloudApi(this.client);
}

/// [CurrencyCloudClient] is used for communication with the CurrencyCloudService. It provides Request methods
/// like [get] and [post] which handle the basic communication overheads like adding authentication headers.
class CurrencyCloudClient {
  static final String devUri = 'https://devapi.currencycloud.com/v2';
  static final String liveUri = 'https://api.currencycloud.com/v2';

  final String baseUri;

  String loginId, apiKey;

  final AuthToken _authToken = AuthToken();

  /// Provide true for [useLiveUri] if you want to use this client in production and trigger real money transfers
  CurrencyCloudClient(this.loginId, this.apiKey, {bool useLiveUri = false})
      : baseUri = useLiveUri ? liveUri : devUri;

  bool get isAuthenticated => _authToken.isSet;

  /// Sets auth headers in provided [headers] and sends HTTP GET request to
  /// given [uri] with [body] set as encoded uri parameters.
  Future<Map<String, dynamic>> get(String uri,
      {Map<String, dynamic>? body, Map<String, String>? headers}) async {
    uri = baseUri + uri;

    headers = await _setAuthHeader(headers);

    if (body != null) {
      // list of the uri parameters to be set
      var params = [];
      for (var key in body.keys) {
        params.add('$key=${body[key]}');
      }

      uri += '?';
      uri += params.join('&');
      uri = Uri.encodeFull(uri);
    }

    log.finest('Sending GET request to URL: ' + uri);
    var response = await http.get(Uri.parse(uri), headers: headers);
    Map<String, dynamic> responseMap;

    try {
      responseMap = _decodeResponse(response);
    } on AuthException {
      log.finest('Got AuthException, reauthenticating');
      await authenticate();
      headers = await _setAuthHeader(headers);

      response = await http.get(Uri.parse(uri), headers: headers);
      responseMap = _decodeResponse(response);
    }

    return responseMap;
  }

  /// Sets auth headers in provided [headers] and sends HTTP POST request to
  /// given [methodUrl].
  Future<Map<String, dynamic>> post(String methodUrl,
      {Map<String, dynamic>? body, Map<String, String>? headers}) async {
    final url = baseUri + methodUrl;

    headers = await _setAuthHeader(headers);
    body ??= {};

    Map<String, dynamic> responseMap;
    var response =
        await http.post(Uri.parse(url), headers: headers, body: body);

    try {
      responseMap = _decodeResponse(response);
    } on AuthException {
      await authenticate();
      headers = await _setAuthHeader(headers);

      response = await http.post(Uri.parse(url), headers: headers, body: body);
      responseMap = _decodeResponse(response);
    }

    return responseMap;
  }

  /// Authenticates this [CurrencyCloud] using its [loginId] and [apiKey]. [CurrencyCloud] will authenticate
  /// automatically if a Request is sent without authenticating or when the Authentication Token expires.
  Future<void> authenticate() async {
    var authUri = baseUri + '/authenticate/api';

    var body = {
      'login_id': loginId,
      'api_key': apiKey,
    };

    log.finest('Sending authentication request to url: $authUri');
    log.finest('body: $body');

    var response = await http.post(Uri.parse(authUri), body: body);
    var responseMap = _decodeResponse(response);

    log.finest('Got response Token: ${responseMap['auth_token']}');

    _authToken.value = responseMap['auth_token'] as String?;
  }

  /// Closes authenticated Session
  Future<void> closeSession() async {
    var uri = baseUri + '/authenticate/close_session';

    await http.post(Uri.parse(uri));
    _authToken.reset;
  }

  Map<String, dynamic> _decodeResponse(http.Response response) {
    var responseBody = jsonDecode(response.body);
    if (responseBody is! Map) {
      throw CurrencyCloudException(
          response.statusCode, responseBody.toString());
    }
    Map<String, dynamic> responseMap = responseBody as Map<String, dynamic>;
    if (responseMap.containsKey('error_code')) {
      final errorMessage = jsonEncode(responseMap['error_messages']);
      if (responseBody['error_code'] == 'auth_failed') {
        throw AuthException(response.statusCode, errorMessage);
      }
      throw CurrencyCloudException(response.statusCode, errorMessage);
    }
    if (response.statusCode < 200 || response.statusCode >= 300) {
      throw CurrencyCloudException(response.statusCode, 'No error message');
    }
    return responseMap;
  }

  /// Returns a copy of given [headers] with the 'X-Auth-Token' header set if authToken has been set before. If
  /// [headers] is not being provided returns a [headers] Map only containing the set 'X-Auth-Token'.
  Future<Map<String, String>> _setAuthHeader(
      [Map<String, String>? headers]) async {
    headers ??= <String, String>{};
    var newHeaders = Map<String, String>.from(headers);

    if (_authToken.isSet) {
      newHeaders['X-Auth-Token'] = _authToken.value!;
    } else {
      await authenticate();
      newHeaders['X-Auth-Token'] = _authToken.value!;
    }

    return newHeaders;
  }
}

class CurrencyCloudException implements Exception {
  final String name = 'CurrencyCloudException';
  final int statusCode;
  final String message;
  final String? requestUri;

  CurrencyCloudException(this.statusCode, this.message, {this.requestUri});

  @override
  String toString() => '$name ($statusCode): $message';
}

class AuthException extends CurrencyCloudException {
  @override
  // ignore: overridden_fields
  final String name = 'AuthException';

  AuthException(int statusCode, String message) : super(statusCode, message);
}

class AuthToken {
  String? value;
  bool get isSet => value != null;

  void reset() {
    value = null;
  }
}
