part of currency_cloud;

class BalancesApi extends CurrencyCloudApi {
  BalancesApi(CurrencyCloudClient client) : super(client);

  /// Retrieves the balance for the given currency code (ISO 4217)
  Future<Balance> retrieve(String currency) async {
    final json =
        await client.get('/balances/${currency.toUpperCase()}', body: {});
    return Balance.fromJson(json);
  }
}
