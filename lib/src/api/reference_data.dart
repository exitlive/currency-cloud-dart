part of currency_cloud;

class ReferenceDataApi extends CurrencyCloudApi {
  ReferenceDataApi(CurrencyCloudClient client) : super(client);

  /// Returns required beneficiary details and their basic validation formats
  /// [currency] is the currency code (ISO 4217) of the beneficiary bank account
  /// [bankAccountCountry] is the country of the beneficiary bank account
  /// [beneficiaryCountry] is the nationality of the beneficiary
  Future<Map<String, List>> beneficiaryRequiredDetails(
      {String? currency,
      String? bankAccountCountry,
      String? beneficiaryCountry}) async {
    var uri = '/reference/beneficiary_required_details';
    var body = <String, String>{
      if (currency != null) 'currency': currency,
      if (bankAccountCountry != null)
        'bank_account_country': bankAccountCountry,
      if (beneficiaryCountry != null) 'beneficiary_country': beneficiaryCountry
    };

    final result = await client.get(uri, body: body.isEmpty ? null : body);

    return result.cast<String, List>();
  }
}
