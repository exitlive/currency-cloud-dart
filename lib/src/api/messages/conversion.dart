part of '../../../messages.dart';

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class CreateConversion {
  final String buyCurrency;
  final String sellCurrency;
  final FixedSide fixedSide;
  @JsonKey(toJson: decimalNonNullToJson, fromJson: decimalNonNullFromJson)
  final Decimal amount;
  @JsonKey(toJson: boolNonNullToJson, fromJson: boolNonNullFromJson)
  final bool termAgreement;

  final String? reason;

  CreateConversion({
    required this.buyCurrency,
    required this.sellCurrency,
    required this.fixedSide,
    required this.amount,
    required this.termAgreement,
    this.reason,
  });

  factory CreateConversion.fromJson(Map<String, dynamic> json) =>
      _$CreateConversionFromJson(json);

  Map<String, dynamic> toJson() => _$CreateConversionToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class Conversion {
  final String id;
  final DateTime? settlementDate;
  final DateTime? conversionDate;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? status;
  final String? buyCurrency;
  final String? sellCurrency;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  final Decimal? clientBuyAmount;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  final Decimal? clientSellAmount;
  final FixedSide? fixedSide;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  final Decimal? coreRate;

  Conversion({
    required this.id,
    this.settlementDate,
    this.conversionDate,
    this.createdAt,
    this.updatedAt,
    this.status,
    this.buyCurrency,
    this.sellCurrency,
    this.clientBuyAmount,
    this.clientSellAmount,
    this.fixedSide,
    this.coreRate,
  });

  factory Conversion.fromJson(Map<String, dynamic> json) =>
      _$ConversionFromJson(json);

  Map<String, dynamic> toJson() => _$ConversionToJson(this);
}
