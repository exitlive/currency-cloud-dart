part of '../../../messages.dart';

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class CreatePayment {
  final String currency;
  final String beneficiaryId;
  @JsonKey(toJson: decimalNonNullToJson, fromJson: decimalNonNullFromJson)
  final Decimal amount;
  final String reason;
  final String reference;

  final String? paymentDate;
  final PaymentType? paymentType;
  final String? conversionId;
  final EntityType? payerEntityType;
  final String? payerCompanyName;
  final String? payerFirstName;
  final String? payerLastName;
  final String? payerCity;
  final String? payerAddress;
  final String? payerPostcode;
  final String? payerStateOrProvince;
  final String? payerCountry;
  final String? payerDateOfBirth;
  final IdentificationType? payerIdentificationType;
  final String? payerIdentificationValue;
  final String? uniqueRequestId;
  final String? ultimateBeneficiaryName;
  final String? onBehalfOf;

  CreatePayment({
    required this.currency,
    required this.beneficiaryId,
    required this.amount,
    required this.reason,
    required this.reference,
    this.paymentDate,
    this.paymentType = PaymentType.regular,
    this.conversionId,
    this.payerEntityType,
    this.payerCompanyName,
    this.payerFirstName,
    this.payerLastName,
    this.payerCity,
    this.payerAddress,
    this.payerPostcode,
    this.payerStateOrProvince,
    this.payerCountry,
    this.payerDateOfBirth,
    this.payerIdentificationType,
    this.payerIdentificationValue,
    this.uniqueRequestId,
    this.ultimateBeneficiaryName,
    this.onBehalfOf,
  });

  factory CreatePayment.fromJson(Map<String, dynamic> json) =>
      _$CreatePaymentFromJson(json);

  Map<String, dynamic> toJson() => _$CreatePaymentToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class Payment {
  final String id;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  final Decimal? amount;
  final String? beneficiaryId;
  final String? currency;
  final String? reference;
  final String? reason;
  final String? status;
  final String? creatorContactId;
  final PaymentType? paymentType;
  final String? paymentDate;
  final String? transferredAt;
  final String? authorisationStepsRequired;
  final String? lastUpdaterContactId;
  final String? shortReference;
  final String? conversionId;
  final String? failureReason;
  final String? payerId;
  final String? payerDetailsSource;
  final DateTime? createdAt;
  final DateTime? updatedAt;
  final String? paymentGroupId;
  final String? uniqueRequestId;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  final Decimal? failureReturnedAmount;
  final String? ultimateBeneficiaryName;

  Payment({
    required this.id,
    this.amount,
    this.beneficiaryId,
    this.currency,
    this.reference,
    this.reason,
    this.status,
    this.creatorContactId,
    this.paymentType,
    this.paymentDate,
    this.transferredAt,
    this.authorisationStepsRequired,
    this.lastUpdaterContactId,
    this.shortReference,
    this.conversionId,
    this.failureReason,
    this.payerId,
    this.payerDetailsSource,
    this.createdAt,
    this.updatedAt,
    this.paymentGroupId,
    this.uniqueRequestId,
    this.failureReturnedAmount,
    this.ultimateBeneficiaryName,
  });

  factory Payment.fromJson(Map<String, dynamic> json) =>
      _$PaymentFromJson(json);

  Map<String, dynamic> toJson() => _$PaymentToJson(this);
}
