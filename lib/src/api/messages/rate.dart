part of '../../../messages.dart';

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class DetailedRateRequest {
  final String buyCurrency;
  final String sellCurrency;
  final FixedSide fixedSide;
  @JsonKey(toJson: decimalNonNullToJson, fromJson: decimalNonNullFromJson)
  final Decimal amount;
  final String? onBehalfOf;
  final DateTime? conversionDate;

  DetailedRateRequest({
    required this.buyCurrency,
    required this.sellCurrency,
    required this.fixedSide,
    required this.amount,
    this.onBehalfOf,
    this.conversionDate,
  });

  factory DetailedRateRequest.fromJson(Map<String, dynamic> json) =>
      _$DetailedRateRequestFromJson(json);

  Map<String, dynamic> toJson() => _$DetailedRateRequestToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class Rate {
  DateTime? settlementCutOffTime;
  String? currencyPair;

  /// Currency code (ISO 4217)
  String? clientBuyCurrency;

  /// Currency code (ISO 4217)
  String? clientSellCurrency;

  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? clientBuyAmount;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? clientSellAmount;
  FixedSide? fixedSide;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? clientRate;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? partnerRate;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? coreRate;
  @JsonKey(toJson: boolToJson, fromJson: boolFromJson)
  bool? depositRequired;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? depositAmount;

  /// Currency code (ISO 4217)
  String? depositCurrency;
  @JsonKey(toJson: decimalToJson, fromJson: decimalFromJson)
  Decimal? midMarketRate;

  Rate();

  factory Rate.fromJson(Map<String, dynamic> json) => _$RateFromJson(json);

  Map<String, dynamic> toJson() => _$RateToJson(this);
}
