part of '../../../messages.dart';

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class Balance {
  final String id;
  final String accountId;

  /// Currency code (ISO 4217)
  final String currency;

  @JsonKey(toJson: decimalNonNullToJson, fromJson: decimalNonNullFromJson)
  final Decimal amount;
  final DateTime createdAt;
  final DateTime? updatedAt;

  Balance({
    required this.id,
    required this.accountId,
    required this.currency,
    required this.amount,
    required this.createdAt,
    this.updatedAt,
  });

  factory Balance.fromJson(Map<String, dynamic> json) =>
      _$BalanceFromJson(json);

  Map<String, dynamic> toJson() => _$BalanceToJson(this);
}
