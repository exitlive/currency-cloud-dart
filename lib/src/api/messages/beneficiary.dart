part of '../../../messages.dart';

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class CreateBeneficiary {
  final String name;
  final String bankAccountHolderName;
  final String bankCountry;

  /// Currency code (ISO 4217)
  final String currency;
  final String? email;
  final String? beneficiaryAddress;
  final String? beneficiaryCountry;
  final String? accountNumber;
  @JsonKey(name: 'routing_code_type_1')
  final RoutingCodeType? routingCodeType1;
  @JsonKey(name: 'routing_code_value_1')
  final String? routingCodeValue1;
  @JsonKey(name: 'routing_code_type_2')
  final RoutingCodeType? routingCodeType2;
  @JsonKey(name: 'routing_code_value_2')
  final String? routingCodeValue2;
  final String? bicSwift;
  final String? iban;
  @JsonKey(toJson: boolToJson, fromJson: boolFromJson)
  final bool? defaultBeneficiary;
  final String? bankAddress;
  final String? bankName;
  final BankAccountType? bankAccountType;
  final EntityType? beneficiaryEntityType;
  final String? beneficiaryCompanyName;
  final String? beneficiaryFirstName;
  final String? beneficiaryLastName;
  final String? beneficiaryCity;
  final String? beneficiaryPostcode;
  final String? beneficiaryStateOrProvince;
  final String? beneficiaryDateOfBirth;
  final EntityType? beneficiaryIdentificationType;
  final String? beneficiaryIdentificationValue;
  final List<PaymentType?>? paymentTypes;
  final String? onBehalfOf;

  CreateBeneficiary({
    required this.name,
    required this.bankAccountHolderName,
    required this.bankCountry,
    required this.currency,
    this.email,
    this.beneficiaryAddress,
    this.beneficiaryCountry,
    this.accountNumber,
    this.routingCodeType1,
    this.routingCodeValue1,
    this.routingCodeType2,
    this.routingCodeValue2,
    this.bicSwift,
    this.iban,
    this.defaultBeneficiary,
    this.bankAddress,
    this.bankName,
    this.bankAccountType,
    this.beneficiaryEntityType,
    this.beneficiaryCompanyName,
    this.beneficiaryFirstName,
    this.beneficiaryLastName,
    this.beneficiaryCity,
    this.beneficiaryPostcode,
    this.beneficiaryStateOrProvince,
    this.beneficiaryDateOfBirth,
    this.beneficiaryIdentificationType,
    this.beneficiaryIdentificationValue,
    this.paymentTypes,
    this.onBehalfOf,
  });

  factory CreateBeneficiary.fromJson(Map<String, dynamic> json) =>
      _$CreateBeneficiaryFromJson(json);

  Map<String, dynamic> toJson() => _$CreateBeneficiaryToJson(this);
}

@JsonSerializable(fieldRename: FieldRename.snake, includeIfNull: false)
class Beneficiary {
  final String id;
  final String bankAccountHolderName;
  final String name;
  final String? email;
  final List<PaymentType?>? paymentTypes;
  final List<String>? beneficiaryAddress;
  final String? beneficiaryCountry;
  final EntityType? beneficiaryEntityType;
  final String? beneficiaryCompanyName;
  final String? beneficiaryFirstName;
  final String? beneficiaryLastName;
  final String? beneficiaryCity;
  final String? beneficiaryPostcode;
  final String? beneficiaryStateOrProvince;
  final String? beneficiaryDateOfBirth;
  final EntityType? beneficiaryIdentificationType;
  final String? beneficiaryIdentificationValue;
  final String? bankCountry;
  final String? bankName;
  final List<String>? bankAddress;
  final BankAccountType? bankAccountType;
  final String? onBehalfOf;

  /// Currency code (ISO 4217)
  final String? currency;
  final String? accountNumber;
  @JsonKey(name: 'routing_code_type_1')
  final RoutingCodeType? routingCodeType1;
  @JsonKey(name: 'routing_code_value_1')
  final String? routingCodeValue1;
  @JsonKey(name: 'routing_code_type_2')
  final RoutingCodeType? routingCodeType2;
  @JsonKey(name: 'routing_code_value_2')
  final String? routingCodeValue2;
  final String? bicSwift;
  final String? iban;
  @JsonKey(toJson: boolToJson, fromJson: boolFromJson)
  final bool? defaultBeneficiary;
  final String? creatorContactId;
  final String? beneficiaryExternalReference;
  final DateTime? createdAt;
  final DateTime? updatedAt;

  Beneficiary({
    required this.id,
    required this.bankAccountHolderName,
    required this.name,
    this.email,
    this.paymentTypes,
    this.beneficiaryAddress,
    this.beneficiaryCountry,
    this.beneficiaryEntityType,
    this.beneficiaryCompanyName,
    this.beneficiaryFirstName,
    this.beneficiaryLastName,
    this.beneficiaryCity,
    this.beneficiaryPostcode,
    this.beneficiaryStateOrProvince,
    this.beneficiaryDateOfBirth,
    this.beneficiaryIdentificationType,
    this.beneficiaryIdentificationValue,
    this.bankCountry,
    this.bankName,
    this.bankAddress,
    this.bankAccountType,
    this.onBehalfOf,
    this.currency,
    this.accountNumber,
    this.routingCodeType1,
    this.routingCodeValue1,
    this.routingCodeType2,
    this.routingCodeValue2,
    this.bicSwift,
    this.iban,
    this.defaultBeneficiary,
    this.creatorContactId,
    this.beneficiaryExternalReference,
    this.createdAt,
    this.updatedAt,
  });

  factory Beneficiary.fromJson(Map<String, dynamic> json) =>
      _$BeneficiaryFromJson(json);

  Map<String, dynamic> toJson() => _$BeneficiaryToJson(this);
}
