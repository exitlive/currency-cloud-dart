part of '../../../messages.dart';

String? decimalToJson(Decimal? value) => value?.toString();
Decimal? decimalFromJson(String? value) =>
    value == null ? null : Decimal.parse(value);

String decimalNonNullToJson(Decimal value) => value.toString();
Decimal decimalNonNullFromJson(String value) => Decimal.parse(value);

String? boolToJson(bool? value) =>
    value == null ? null : (value ? 'true' : 'false');
bool? boolFromJson(dynamic value) {
  if (value == null) return null;
  if (value is bool) return value;
  return value.toString().toLowerCase() == 'true';
}

String boolNonNullToJson(bool value) => (value ? 'true' : 'false');
bool boolNonNullFromJson(dynamic value) {
  if (value is bool) return value;
  return value.toString().toLowerCase() == 'true';
}
