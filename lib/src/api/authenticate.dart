part of currency_cloud;

class AuthenticateApi extends CurrencyCloudApi {
  AuthenticateApi(CurrencyCloudClient client) : super(client);

  /// Authenticates this [CurrencyCloud] instance. This [CurrencyCloud] instance can
  /// only be used after authentication.
  Future<void> authenticate() => client.authenticate();

  /// Closes authenticated Session
  Future<void> closeSession() => client.closeSession();
}
