part of currency_cloud;

class RatesApi extends CurrencyCloudApi {
  RatesApi(CurrencyCloudClient client) : super(client);

  Future<Rate> detailed(DetailedRateRequest request) async {
    // Todo: the body of this should be converted to a message.
    final json = await client.get('/rates/detailed', body: request.toJson());
    return Rate.fromJson(json);
  }
}
