part of currency_cloud;

class ConversionsApi extends CurrencyCloudApi {
  ConversionsApi(CurrencyCloudClient client) : super(client);

  Future<Conversion> create(CreateConversion createConversion) async {
    final json = await client.post('/conversions/create',
        body: createConversion.toJson());
    return Conversion.fromJson(json);
  }
}
