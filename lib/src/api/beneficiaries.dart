part of currency_cloud;

class BeneficiariesApi extends CurrencyCloudApi {
  BeneficiariesApi(CurrencyCloudClient client) : super(client);

  Future<Beneficiary> create(CreateBeneficiary createBeneficiary) async {
    final json = await client.post('/beneficiaries/create',
        body: createBeneficiary.toJson());
    return Beneficiary.fromJson(json);
  }

  Future<Beneficiary> retrieve(String beneficiaryId) async {
    final json = await client.get('/beneficiaries/$beneficiaryId');
    return Beneficiary.fromJson(json);
  }
}
