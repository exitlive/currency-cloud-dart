part of currency_cloud;

class PaymentsApi extends CurrencyCloudApi {
  PaymentsApi(CurrencyCloudClient client) : super(client);

  /// [CreatePayment.paymentType] will default to [PaymentType.regular] if not provided
  Future<Payment> create(CreatePayment createPayment) async {
    final json =
        await client.post('/payments/create', body: createPayment.toJson());
    return Payment.fromJson(json);
  }
}
