// Copyright (c) 2015, Daniel Domberger. All rights reserved. Use of this source code
// is governed by a MIT-style license that can be found in the LICENSE file.

/// The currency_cloud library.
///
/// Dart library for the CurrencyCloud service
library currency_cloud;

import 'dart:async';
import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:logging/logging.dart';

import 'messages.dart';

export 'messages.dart';

part 'src/api/authenticate.dart';
part 'src/api/balances.dart';
part 'src/api/beneficiaries.dart';
part 'src/api/conversion.dart';
part 'src/api/payments.dart';
part 'src/api/rates.dart';
part 'src/api/reference_data.dart';
part 'src/base/currency_cloud_base.dart';

final Logger log = Logger('CurrencyCloud');

/// [CurrencyCloud] is the Class that provides the Interface for external calls. Using this library
/// starts by getting a [CurrencyCloud] instance and calling the API methods one wants to use on that.
class CurrencyCloud {
  CurrencyCloudClient client;

  // Public viewable APIs according to CurrencyCloud Docs
  final AuthenticateApi authApi;
  final RatesApi ratesApi;
  final ConversionsApi conversionApi;
  final BalancesApi balancesApi;
  final ReferenceDataApi referenceDataApi;
  final BeneficiariesApi beneficiariesApi;
  final PaymentsApi paymentsApi;

  factory CurrencyCloud(String loginId, String apiKey,
      {bool useLiveUri = false}) {
    final client = CurrencyCloudClient(loginId, apiKey, useLiveUri: useLiveUri);
    return CurrencyCloud._(client, useLiveUri: useLiveUri);
  }

  /// Provide true for [useLiveUri] if you want to use this client in production and trigger real money transfers
  CurrencyCloud._(this.client, {bool useLiveUri = false})
      : authApi = AuthenticateApi(client),
        ratesApi = RatesApi(client),
        conversionApi = ConversionsApi(client),
        balancesApi = BalancesApi(client),
        referenceDataApi = ReferenceDataApi(client),
        beneficiariesApi = BeneficiariesApi(client),
        paymentsApi = PaymentsApi(client);
}
