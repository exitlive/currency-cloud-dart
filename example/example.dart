import 'package:currency_cloud/currency_cloud.dart';
import 'package:decimal/decimal.dart';

void main() async {
  final cc = CurrencyCloud('loginId', 'apiKey');

  // 1. Authenticate
  await cc.authApi.authenticate();

  // 2. Get a Quote
  final buyCurrency = 'EUR';
  final sellCurrency = 'GBP';
  final fixedSide = FixedSide.buy;
  final amount = Decimal.parse('1000.00');

  final rate = await cc.ratesApi.detailed(DetailedRateRequest(
    buyCurrency: buyCurrency,
    sellCurrency: sellCurrency,
    fixedSide: fixedSide,
    amount: amount,
  ));
  print(rate);

  // 3. Convert
  final createConversion = CreateConversion(
    buyCurrency: buyCurrency,
    sellCurrency: sellCurrency,
    fixedSide: fixedSide,
    amount: amount,
    termAgreement: true,
    reason: 'Invoice Payment',
  );

  final conversion = await cc.conversionApi.create(createConversion);
  print(conversion);

  // 4. Add a Beneficiary
  final createBeneficiary = CreateBeneficiary(
    name: 'My Test Beneficiary',
    bankAccountHolderName: 'My Test Account Holder',
    bankCountry: 'DE',
    currency: buyCurrency,
    iban: 'DE89370400440532013000',
    bicSwift: 'COBADEFF',
  );

  final beneficiary = await cc.beneficiariesApi.create(createBeneficiary);
  print(beneficiary);

  // 5. Pay
  final createPayment = CreatePayment(
    currency: buyCurrency,
    beneficiaryId: '9b40b6e7-5d18-42ac-87bc-5c5f774e91ab',
    amount: amount,
    reason: 'This is a test payment.',
    reference: 'This is a test reference.',
  );

  final payment = await cc.paymentsApi.create(createPayment);
  print(payment);
}
