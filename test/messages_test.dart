import 'package:currency_cloud/currency_cloud.dart';
import 'package:decimal/decimal.dart';
import 'package:test/test.dart';

void main() {
  group('decimal', () {
    group('ToJson', () {
      test('returns null if the value is null', () async {
        expect(decimalToJson(null), null);
      });
      test('returns correct decimal', () async {
        expect(decimalToJson(Decimal.parse('10.00')), '10');
        expect(decimalToJson(Decimal.parse('12.345')), '12.345');
      });
    });
    group('FromJson', () {
      test('returns null if the value is null', () async {
        expect(decimalFromJson(null), null);
      });
      test('returns correct decimal', () async {
        expect(decimalFromJson('10.00')!.toDouble(), 10.00);
        expect(decimalFromJson('12.345')!.toDouble(), 12.345);
        expect(decimalFromJson('1')!.toDouble(), 1.00);
      });
      test('throws FormatException for invalid values', () async {
        expect(() => decimalFromJson('FOO'), throwsFormatException);
      });
    });
  });
  group('bool', () {
    group('ToJson', () {
      test('returns null if the value is null', () {
        expect(boolToJson(null), null);
      });
      test('returns correct converted value', () {
        expect(boolToJson(true), 'true');
        expect(boolToJson(false), 'false');
      });
    });
    group('FromJson', () {
      test('returns null if the value is null', () {
        expect(boolFromJson(null), null);
      });
      test('returns correct converted value', () {
        expect(boolFromJson('true'), isTrue);
        expect(boolFromJson('false'), isFalse);
        expect(boolFromJson(true), isTrue);
        expect(boolFromJson(false), isFalse);
      });
      test('ignores case', () async {
        expect(boolFromJson('TRUE'), isTrue);
        expect(boolFromJson('FALSE'), isFalse);
      });
      test('returns false if unknown', () {
        expect(boolFromJson('FOO'), isFalse);
      });
    });
  });
}
