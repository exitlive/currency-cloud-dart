import 'dart:convert';

Map<String, dynamic> getJson(String json) =>
    jsonDecode(json) as Map<String, dynamic>;

final balanceJson = '''
{
  "id": "4e33f858-7f51-477d-9a15-aaa852f2a59e",
  "account_id": "72970a7c-7921-431c-b95f-3438724ba16f",
  "currency": "GBP",
  "amount": "97963.46",
  "created_at": "2018-04-18T12:30:40+00:00",
  "updated_at": "2018-07-12T00:00:04+00:00"
}
''';

final conversionJson = '''
{
    "id": "6fe7f3f1-083e-4c5b-97a5-a9c43bc1e5a4",
    "settlement_date": "2018-07-24T15:30:00+00:00",
    "conversion_date": "2018-07-24T00:00:00+00:00",
    "short_reference": "20180720-DVXMQY",
    "creator_contact_id": "a66ca63f-e668-47af-8bb9-74363240d781",
    "account_id": "72970a7c-7921-431c-b95f-3438724ba16f",
    "currency_pair": "GBPUSD",
    "status": "awaiting_funds",
    "buy_currency": "USD",
    "sell_currency": "GBP",
    "client_buy_amount": "150.00",
    "client_sell_amount": "106.39",
    "fixed_side": "buy",
    "core_rate": "1.4099",
    "partner_rate": "",
    "partner_status": "funds_arrived",
    "partner_buy_amount": "0.00",
    "partner_sell_amount": "0.00",
    "client_rate": "1.4099",
    "deposit_required": false,
    "deposit_amount": "0.00",
    "deposit_currency": "",
    "deposit_status": "not_required",
    "deposit_required_at": "",
    "payment_ids": [],
    "unallocated_funds": "0.00",
    "unique_request_id": null,
    "created_at": "2018-07-20T17:38:33+00:00",
    "updated_at": "2018-07-20T17:38:33+00:00",
    "mid_market_rate": "1.4100"
}
''';

final beneficiaryJson = '''
{
    "id": "8d98ade0-2c79-42a8-9b9b-6faf286c9f07",
    "bank_account_holder_name": "My Test Account Holder",
    "name": "My Test Beneficiary 3",
    "email": null,
    "payment_types": [
        "regular"
    ],
    "beneficiary_address": [],
    "beneficiary_country": null,
    "beneficiary_entity_type": null,
    "beneficiary_company_name": null,
    "beneficiary_first_name": null,
    "beneficiary_last_name": null,
    "beneficiary_city": null,
    "beneficiary_postcode": null,
    "beneficiary_state_or_province": null,
    "beneficiary_date_of_birth": null,
    "beneficiary_identification_type": null,
    "beneficiary_identification_value": null,
    "bank_country": "GB",
    "bank_name": "TEST BANK NAME",
    "bank_account_type": null,
    "currency": "GBP",
    "account_number": "3284734675",
    "routing_code_type_1": "sort_code",
    "routing_code_value_1": "101193",
    "routing_code_type_2": null,
    "routing_code_value_2": null,
    "bic_swift": null,
    "iban": null,
    "default_beneficiary": "false",
    "creator_contact_id": "a66ca63f-e668-47af-8bb9-74363240d781",
    "bank_address": [
        "TEST BANK ADDRESS",
        " NOT USING EXTERNAL SERVICE",
        " DEVELOPMENT ENVIRONMENT."
    ],
    "created_at": "2018-06-19T14:58:04+00:00",
    "updated_at": "2018-06-19T14:58:04+00:00",
    "beneficiary_external_reference": null
}
''';

final paymentJson = '''
{
    "id": "66d33d1a-158c-4f38-8d8e-ab144d8a4e11",
    "amount": "10.00",
    "beneficiary_id": "9b40b6e7-5d18-42ac-87bc-5c5f774e91ab",
    "currency": "GBP",
    "reference": "This is a test reference.",
    "reason": "This is a test payment.",
    "status": "ready_to_send",
    "creator_contact_id": "a66ca63f-e668-47af-8bb9-74363240d781",
    "payment_type": "regular",
    "payment_date": "2018-07-23",
    "transferred_at": "",
    "authorisation_steps_required": "0",
    "last_updater_contact_id": "a66ca63f-e668-47af-8bb9-74363240d781",
    "short_reference": "180720-QWJWJZ001",
    "conversion_id": null,
    "failure_reason": "",
    "payer_id": "68561f01-b5d8-4fad-9bcb-d1712a1bc0c8",
    "payer_details_source": "account",
    "created_at": "2018-07-20T17:38:38+00:00",
    "updated_at": "2018-07-20T17:38:38+00:00",
    "payment_group_id": null,
    "unique_request_id": null,
    "failure_returned_amount": "0.00",
    "ultimate_beneficiary_name": null
}
''';

final rateJson = '''
{
    "settlement_cut_off_time": "2018-07-24T13:00:00Z",
    "currency_pair": "GBPUSD",
    "client_buy_currency": "GBP",
    "client_sell_currency": "USD",
    "client_buy_amount": "100.00",
    "client_sell_amount": "141.01",
    "fixed_side": "buy",
    "client_rate": "1.4101",
    "partner_rate": null,
    "core_rate": "1.4101",
    "deposit_required": false,
    "deposit_amount": "0.0",
    "deposit_currency": "USD",
    "mid_market_rate": "1.4100"
}
''';
