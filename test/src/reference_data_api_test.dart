library currency_cloud.reference_data_api_test;

import 'dart:async';

import 'package:currency_cloud/currency_cloud.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'mock_client.dart';

void main() {
  group('reference data api', () {
    late ReferenceDataApi referenceDataApi;
    late MockClient mockClient;

    setUp(() {
      mockClient = MockClient();
      referenceDataApi = ReferenceDataApi(mockClient);
      referenceDataApi.client = mockClient;
    });

    test(
        'call to beneficiaryRequiredDetails without arguments should do according GET call',
        () async {
      final uri = '/reference/beneficiary_required_details';

      when(() => mockClient.get(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value({}));
      await referenceDataApi.beneficiaryRequiredDetails();
      verify(() => mockClient.get(uri)).called(1);
    });

    test(
        'call to beneficiaryRequiredDetails with arguments should do according GET call',
        () async {
      final uri = '/reference/beneficiary_required_details';

      when(() => mockClient.get(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value({}));
      await referenceDataApi.beneficiaryRequiredDetails(
          currency: 'EUR', bankAccountCountry: 'AT', beneficiaryCountry: 'DE');
      verify(() => mockClient.get(uri, body: {
            'currency': 'EUR',
            'bank_account_country': 'AT',
            'beneficiary_country': 'DE'
          })).called(1);
    });
  });
}
