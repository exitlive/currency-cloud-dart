library currency_cloud.rates_api_test;

import 'dart:async';

import 'package:currency_cloud/currency_cloud.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'mock_client.dart';
import 'test_data.dart';

void main() {
  group('balances', () {
    late BalancesApi balancesApi;
    late MockClient mockClient;

    setUp(() {
      mockClient = MockClient();
      balancesApi = BalancesApi(mockClient);
      balancesApi.client = mockClient;
    });

    test('getting the balance should return the correct response', () async {
      final currency = 'USD';

      final json = getJson(balanceJson);

      final uri = '/balances/USD';

      when(() => mockClient.get(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value(json));
      final response = await balancesApi.retrieve(currency);
      verify(() => mockClient.get(uri, body: {})).called(1);
      expect(response, TypeMatcher<Balance>());
      expect(response.id, json['id']);
      // More message tests in balance_test
    });
  });
}
