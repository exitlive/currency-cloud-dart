library currency_cloud.mock_client;

import 'package:currency_cloud/currency_cloud.dart';
import 'package:mocktail/mocktail.dart';

class MockClient extends Mock implements CurrencyCloudClient {}
