library currency_cloud.beneficiaries_api_test;

import 'dart:async';

import 'package:currency_cloud/currency_cloud.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'mock_client.dart';
import 'test_data.dart';

void main() {
  group('BeneficiariesApi', () {
    late BeneficiariesApi beneficiariesApi;
    late MockClient mockClient;

    setUp(() {
      mockClient = MockClient();
      beneficiariesApi = BeneficiariesApi(mockClient);
      beneficiariesApi.client = mockClient;
    });

    test('call with all parameters set to valid values', () async {
      final createBeneficiary = CreateBeneficiary(
        name: 'My Test Beneficiary',
        bankAccountHolderName: 'My Test Account Holder',
        bankCountry: 'GB',
        currency: 'GBP',
      );

      final uri = '/beneficiaries/create';

      final json = getJson(beneficiaryJson);
      when(() => mockClient.post(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value(json));
      final beneficiary = await beneficiariesApi.create(createBeneficiary);
      verify(() => mockClient.post(uri, body: any(named: 'body'))).called(1);
      expect(beneficiary.id, json['id']);
      // More values tested in message tests.
    });
  });
}
