library currency_cloud.rates_api_test;

import 'dart:async';

import 'package:currency_cloud/currency_cloud.dart';
import 'package:decimal/decimal.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'mock_client.dart';
import 'test_data.dart';

void main() {
  group('rates', () {
    late RatesApi ratesApi;
    late MockClient mockClient;

    setUp(() {
      mockClient = MockClient();
      ratesApi = RatesApi(mockClient);
      ratesApi.client = mockClient;
    });

    test('call with all parameters set to valid values', () async {
      final uri = '/rates/detailed';

      final json = getJson(rateJson);
      when(() => mockClient.get(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value(json));
      final request = DetailedRateRequest(
        buyCurrency: 'EUR',
        sellCurrency: 'USD',
        fixedSide: FixedSide.buy,
        amount: Decimal.parse('12.34'),
      );

      final rate = await ratesApi.detailed(request);
      verify(() => mockClient.get(uri, body: any(named: 'body'))).called(1);
      expect(rate.clientBuyAmount,
          decimalFromJson(json['client_buy_amount'] as String?));
      // More values tested in message tests.
    });
  });
}
