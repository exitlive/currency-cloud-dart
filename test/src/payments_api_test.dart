library currency_cloud.conversion_api_test;

import 'dart:async';

import 'package:currency_cloud/currency_cloud.dart';
import 'package:decimal/decimal.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'mock_client.dart';
import 'test_data.dart';

void main() {
  group(('payments api test'), () {
    late PaymentsApi paymentsApi;
    late MockClient mockClient;

    setUp(() {
      mockClient = MockClient();
      paymentsApi = PaymentsApi(mockClient);
      paymentsApi.client = mockClient;
    });

    test('call with all parameters set to valid values', () async {
      final createPayment = CreatePayment(
        currency: 'GBP',
        beneficiaryId: '9b40b6e7-5d18-42ac-87bc-5c5f774e91ab',
        amount: Decimal.parse('10.00'),
        reason: 'This is a test payment.',
        reference: 'This is a test reference.',
      );

      final uri = '/payments/create';

      final json = getJson(paymentJson);
      when(() => mockClient.post(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value(json));
      final conversion = await paymentsApi.create(createPayment);
      verify(() => mockClient.post(uri, body: any(named: 'body'))).called(1);
      expect(conversion.id, json['id']);
      // More values tested in message tests.
    });
  });
}
