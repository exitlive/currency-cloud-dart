library currency_cloud.conversion_api_test;

import 'dart:async';

import 'package:currency_cloud/currency_cloud.dart';
import 'package:decimal/decimal.dart';
import 'package:mocktail/mocktail.dart';
import 'package:test/test.dart';

import 'mock_client.dart';
import 'test_data.dart';

void main() {
  group(('conversion api test'), () {
    late ConversionsApi conversionApi;
    late MockClient mockClient;

    setUp(() {
      mockClient = MockClient();
      conversionApi = ConversionsApi(mockClient);
      conversionApi.client = mockClient;
    });

    test('call with all parameters set to valid values', () async {
      final createConversion = CreateConversion(
        buyCurrency: 'EUR',
        sellCurrency: 'GBP',
        fixedSide: FixedSide.buy,
        amount: Decimal.parse('38.00'),
        termAgreement: true,
        reason: 'Want to buy',
      );

      final uri = '/conversions/create';

      final json = getJson(conversionJson);
      when(() => mockClient.post(any(), body: any(named: 'body')))
          .thenAnswer((_) => Future.value(json));
      final conversion = await conversionApi.create(createConversion);
      verify(() => mockClient.post(uri, body: any(named: 'body'))).called(1);
      expect(conversion.id, json['id']);
      // More values tested in message tests.
    });
  });
}
