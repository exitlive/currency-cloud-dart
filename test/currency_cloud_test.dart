import 'package:currency_cloud/currency_cloud.dart';
import 'package:test/test.dart';

void main() {
  group('CurrencyCloud', () {
    test('factory constructor should properly create the client', () async {
      final currencyCloud = CurrencyCloud('login', 'apiKey');
      expect(currencyCloud.client, isNotNull);
      expect(currencyCloud.authApi.client, currencyCloud.client);
      expect(currencyCloud.balancesApi.client, currencyCloud.client);
      expect(currencyCloud.beneficiariesApi.client, currencyCloud.client);
      expect(currencyCloud.paymentsApi.client, currencyCloud.client);
      expect(currencyCloud.ratesApi.client, currencyCloud.client);
      expect(currencyCloud.referenceDataApi.client, currencyCloud.client);
    });
  });
}
