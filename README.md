# Currency Cloud Dart API

[![Gitlab Status](https://gitlab.com/exitlive/currency-cloud-dart/badges/master/build.svg)](https://gitlab.com/exitlive/currency-cloud-dart/pipelines)
[![Pub Status](https://img.shields.io/pub/v/currency_cloud.svg)](https://pub.dartlang.org/packages/currency_cloud)


A dart library for the [Currency Cloud](https://developer.currencycloud.com/) service

## Usage

A simple [usage example](example/example.dart) 